<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Api\File\Controllers\FilesController;
use App\Http\Api\Angel\Controllers\AngelController;
use App\Http\Api\Aidant\Controllers\AidantController;
use App\Http\Api\Rappel\Controllers\RappelController;
use App\Http\Api\Feedback\Controllers\FeedBackController;
use App\Http\Api\Emergency\Controllers\EmergencyController;
use App\Http\Api\FicheMedicale\Controllers\FicheMedicaleController;
use App\Http\Api\Notification\Controllers\NotificationController;

/* ================
    Aidant
=================*/

Route::group(['prefix' => '/aidant'], function () {
    // Authentification
    Route::post('/login', [AidantController::class, 'login'])->name('api.aidant.connect');
    Route::post('/register', [AidantController::class, 'register'])->name('api.aidant.register');

    // When connected
    Route::group(['middleware' => ['auth:aidantBearer']], function () {
        // Account
        Route::get('/infos', [AidantController::class, 'getAccount'])->name('api.aidant.getUserInfos');
        Route::post('/delete', [AidantController::class, 'deleteAccount'])->name('api.aidant.deleteAidant');
        Route::post('/update', [AidantController::class, 'updateAccount'])->name('api.aidant.updateProfil');

        // Angels
        Route::group(['prefix' => '/angels'], function () {
            // Angels linked
            Route::get('/list', [AidantController::class, 'getAngels'])->name('api.aidant.getAngels');
            Route::post('/add', [AidantController::class, 'createAngel'])->name('api.angel.register');

            // Account
            Route::post('/{angel}/update', [AidantController::class, 'updateAngel'])->name('api.angel.updateProfil');
            Route::post('/{angel}/update/setting', [AidantController::class, 'updateAngelSetting'])->name('api.angel.updateAngelSetting');
            Route::post('/{angel}/delete', [AidantController::class, 'removeAngelFromAccount'])->name('api.angel.delete');

            // Medical form
            Route::group(['prefix' => '/{angel}/fiche-medicale'], function () {
                Route::post('/update', [FicheMedicaleController::class, 'update']);
                Route::post('/get', [FicheMedicaleController::class, 'aidantGet']);
            });

            // Authentification
            Route::group(['prefix' => '{angel}/auth'], function () {
                Route::post('/logout', [AidantController::class, 'logoutAngel'])->name('api.angel.logout');
                Route::post('/create-login-token', [AidantController::class, 'createAngelLoginToken'])->name('api.angel.create-login-token');
            });
        });

        // Newsfeed
        Route::post('/actualite', [AidantController::class, 'getNewsFeed'])->name('api.actualite.get');
        
        // Rappels
        Route::group(['prefix' => '/rappels'], function () {
            // Get rappels
            Route::group(['prefix' => '/list'], function () {
                Route::post('/today', [AidantController::class, 'getRappelsToday']);
                Route::post('/interval', [AidantController::class, 'getRappelsInterval']);
                Route::post('/angel', [AidantController::class, 'getRappelsByAngel'])->name('api.aidant.getRappelsByAngel');
            });

            // Manage rappels
            Route::post('/create', [AidantController::class, 'createRappel'])->name('api.rappel.create');
            Route::post('/delete/{rappel}', [AidantController::class, 'deleteRappel'])->name('api.rappel.delete');
            Route::post('/update/{rappel}', [AidantController::class, 'updateRappel'])->name('api.rappel.update');

            // Routines
            Route::group(['prefix' => '/routine'], function () {
                Route::post('/delete/{routine}', [RappelController::class, 'deleteRoutine'])->name('api.routine.delete');
                Route::post('/update/{routine}', [RappelController::class, 'updateRoutine'])->name('api.routine.update');
            });
        });
    });
});

/* ================
	Angel
=================*/
Route::group(['prefix' => '/angel'], function () {
    // Authentification
    Route::post('/login', [AngelController::class, 'login'])->name('api.angel.login');
    Route::get('/exchange-login-token', [AngelController::class, 'exchangeLoginToken'])->name('api.angel.exchange-login-token');

    // When connected
    Route::group(['middleware' => ['auth:angelBearer']], function () {
        // Account
        Route::get('/infos', [AngelController::class, 'getData'])->name('api.angel.getUserInfos');
        Route::get('/rappels', [AngelController::class, 'getRappels'])->name('api.angel.getRappels');

        // Angel is on app
        Route::get('/login-app', [AngelController::class, 'loginApp'])->name('api.angel.login-app');
        Route::get('/logout-app', [AngelController::class, 'logoutApp'])->name('api.angel.logout-app');

        // Emergency
        Route::post('/emergency/send', [EmergencyController::class, 'add']);

        // Medical Form
        Route::post('/fiche-medicale/get', [FicheMedicaleController::class, 'angelGet']);

        // Rappels
        Route::group(['prefix' => '/rappels'], function () {
            // Get rappels
            Route::get('/date/{date}', [AngelController::class, 'getRappelsByDate'])->name('api.angel.getRappelsByDate');
            Route::get('/read/{rappel}', [RappelController::class, 'readRappel'])->name('api.rappel.readRappel');
            
            // Alert rappels
            Route::post('/callback-receive', [AngelController::class, 'callbackReceive'])->name('api.angel.callbackReceive');
            Route::get('/receive/{rappel}', [RappelController::class, 'receive'])->name('api.rappel.receiveRappel');
            Route::get('/validation/{rappel}', [RappelController::class, 'validation'])->name('api.rappel.validate');
            Route::get('/cancel/{rappel}', [RappelController::class, 'cancel'])->name('api.rappel.cancel');
        });
    });
});

/* ================
    Emergency
=================*/

Route::group(['prefix' => '/emergency'], function () {
    Route::get('/view/{id}', [EmergencyController::class, 'helpView'])->name('api.emergency.view');
    Route::get('/develop/view/{id}', [EmergencyController::class, 'helpViewDevelop'])->name('api.develop.emergency.view');
});

/* ===================
    Files
====================*/

Route::group(['prefix' => '/file'], function () {
    Route::post('/upload', [FilesController::class, 'upload'])->name('api.file.upload');
    Route::post('/get/{fileReference}', [FilesController::class, 'get'])->name('api.file.get');
    Route::get('/view/{fileReference}', [FilesController::class, 'view'])->name('api.file.view');
});

/* ================
    Feedbacks
=================*/

Route::group(['prefix' => '/feedback'], function () {
    Route::post('/add', [FeedBackController::class, 'add'])->name('api.feedback.add');
});

/* ================
    Notifications
=================*/

Route::group(['prefix' => '/notification'], function () {
    Route::post('/set-token', [NotificationController::class, 'setToken'])->name('api.feedback.add');
});
