<?php

namespace App\Http\Api\Feedback\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddFeedbackRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'nom' => ['required'],
            'email' => ['required'],
            'sujet' => ['required'],
            'message' => ['required'],
        ];
    }

    public function messages() {
        return [
            'nom.required' => 'FEEDBACK_NOM_REQUIRED',
            'email.required' => 'FEEDBACK_EMAIL_REQUIRED',
            'sujet.required' => 'FEEDBACK_SUJET_REQUIRED',
            'message.required' => 'FEEDBACK_MESSAGE_REQUIRED',
        ];
    }
}
