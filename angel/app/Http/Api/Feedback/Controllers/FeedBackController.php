<?php

namespace App\Http\Api\Feedback\Controllers;

use App\Models\Feedback;
use App\Http\Api\Controllers\ApiController;
use App\Http\Api\Feedback\Requests\AddFeedbackRequest;

class FeedBackController extends ApiController
{
    public function add(AddFeedbackRequest $request)
    {
        $feedback = new Feedback();
        $feedback->fill($request->all());
        $feedback->save();

        return $feedback;
    }
}
