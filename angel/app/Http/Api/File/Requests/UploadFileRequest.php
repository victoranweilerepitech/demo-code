<?php

namespace App\Http\Api\File\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadFileRequest extends FormRequest
{
    public function rules() {
        return [
            'file' => ['required'],
        ];
    }

    public function messages() {
        return [
            'file.required' => 'UPLOAD_FILE_REQUIRED',
        ];
    }
}
