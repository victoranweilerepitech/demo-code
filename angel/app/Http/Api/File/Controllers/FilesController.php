<?php

namespace App\Http\Api\File\Controllers;

use \Carbon\Carbon;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


use App\Http\Api\Controllers\ApiController;
use App\Http\Api\File\Requests\UploadFileRequest;

class FilesController extends ApiController
{
    private function randomId($length = 8) {
        $random = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
        return $random;
    }
    
    public function upload(UploadFileRequest $request)
    {
        // Get File
        $file = $request->file('file');
        // Rename file
        $filename = str_replace(' ', '_', time().'_'.$file->getClientOriginalName());
        $fileReference = 'file_'.$this->randomId();
        // Define file url
        $fileUrl = route('api.file.view', $fileReference);
        // Path where file will be uploaded
        $filepath =  'uploaded/'.Carbon::now()->format('m-Y');
        if ($request->header('develop')) {
            $fileUrl = $fileUrl.'?develop=1';
            $filepath =  'uploaded/develop/'.Carbon::now()->format('m-Y');
        }
        
        // Upload file
        $path = $file->storeAs($filepath, $filename);

        // Save file in database
        $fileDb = new File;
        $fileDb->reference = $fileReference;
        $fileDb->type = $file->extension();
        $fileDb->file_name = $filename;
        $fileDb->file_path = $path;
        $fileDb->file_url = $fileUrl;
        $fileDb->meta = $request->meta;
        $fileDb->save();

        return $fileDb;
    }

    public function get($fileReference)
    {
        // Get file informations in database
        $file = File::where('reference', $fileReference)->firstOrFail();
        return $file;
    }

    public function view($fileReference, Request $request)
    {
        // Switch database between dev and prod environment
        if ($request->develop) {
            $file = File::on('mysql_develop')->where('reference', $fileReference)->firstOrFail();
        } else {
            $file = File::where('reference', $fileReference)->firstOrFail();
        }

        // Get file path
        $path = Storage::path($file->file_path);

        // Return file
        if (file_exists($path)) {
            $file = \Illuminate\Support\Facades\File::get($path);
            $type = \Illuminate\Support\Facades\File::mimeType($path);
            $response = \Illuminate\Support\Facades\Response::make($file, 200);
            $response->header("Content-Type", $type);
    
            return $response;
        } else {
            abort(404);
        }
    }
}
