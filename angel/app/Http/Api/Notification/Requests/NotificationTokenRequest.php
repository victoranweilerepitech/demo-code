<?php

namespace App\Http\Api\Notification\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationTokenRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'token' => ['required'],
        ];
    }

    public function messages() {
        return [
            'token.required' => 'NOTIFICATION_TOKEN_REQUIRED',
        ];
    }
}
