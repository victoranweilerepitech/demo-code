<?php

namespace App\Http\Api\Notification\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Api\Controllers\ApiController;
use App\Http\Api\Notification\Requests\NotificationTokenRequest;
use App\Models\Angel;
use App\Models\NotificationToken;

class NotificationController extends ApiController
{
    public function setToken(NotificationTokenRequest $request)
    {
        if (!$request->aidant_id && !$request->angel_id) {
            return $this->responseError('NOTIFICATION_TOKEN_ANGEL_AIDANT_EMPTY');
        }
        if ($request->aidant_id && $request->angel_id) {
            return $this->responseError('NOTIFICATION_TOKEN_ANGEL_AIDANT_BOTH');
        }

        $angelAidantFound = false;

        if ($request->angel_id) {
            $angelToken = NotificationToken::where('angel_id', $request->angel_id)->first();
            if ($angelToken) {
                $angelAidantFound = true;
                $angelToken->token = $request->token;
                $angelToken->save();
                return $angelToken;
            }
        }

        if ($request->aidant_id) {
            $aidantToken = NotificationToken::where('aidant_id', $request->aidant_id)->first();
            if ($aidantToken) {
                $angelAidantFound = true;
                $aidantToken->token = $request->token;
                $aidantToken->save();
                return $aidantToken;
            }
        }

        if (!$angelAidantFound) {
            $notificationToken = new NotificationToken;
            $notificationToken->fill($request->all());
            $notificationToken->save();
            return $notificationToken;
        }
    }
}
