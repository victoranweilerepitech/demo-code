<?php

namespace App\Http\Api;

trait DefaultToolBox 
{
    public function responseError($codes)
    {
        if (!is_array($codes)) {
            $codes = [$codes];
        }
        return response($codes, 445);
    }

    public function sendNotification($token, $title, $content, $data)
    {
        $response = \Illuminate\Support\Facades\Http::post('https://exp.host/--/api/v2/push/send', [
            'to' => $token,
            'sound' => 'default',
            'title' => $title,
            'body' => $content,
            'data' => $data,
        ]);
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $responseData = json_decode($response->body())->data;
            return $responseData;
        }
    }

    function randomToken($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}
