<?php

namespace App\Http\Api\Emergency\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmergencyAddRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'longitude' => ['required'],
            'latitude' => ['required'],
        ];
    }

    public function messages() {
        return [
            'longitude.required' => 'EMERGENCY_ADD_LONGITUDE_REQUIRED',
            'latitude.required' => 'EMERGENCY_ADD_LATITUDE_REQUIRED',
        ];
    }
}
