<?php

namespace App\Http\Api\Emergency\Controllers;

use App\Http\Api\Angel\Repositories\AngelRepository;
use App\Http\Api\Controllers\ApiController;
use App\Http\Api\Emergency\Requests\EmergencyAddRequest;
use App\Mail\EmergencyAlertEmail;
use App\Models\Emergency;

class EmergencyController extends ApiController
{
    public function __construct()
    {
        $this->angelRepository = new AngelRepository;
    }

    public function helpView($id)
    {
        // Get emergency data
        $emergency = Emergency::where('ida', $id)->firstOrFail();
        return view('api.emergency.view', compact('emergency'));
    }

    public function helpViewDevelop($id)
    {
        // Get emergency data
        $emergency = Emergency::on('mysql_develop')->where('ida', $id)->firstOrFail();
        return view('api.emergency.view', compact('emergency'));
    }

    private function phoneFormat($phone)
    {
        // Transform phone number for vonage
        $endPhone = null;
        // Example: 0789898989
        if ((strlen($phone) == 10 && mb_substr($phone, 0, 1) == '0')) {
            $endPhone = '33'.substr($phone, 1);
        }
        // Example: +33789898989
        if ((strlen($phone) == 12 && mb_substr($phone, 0, 3) == '+33')) {
            $endPhone = substr($phone, 1);
        }
        // If phone has the right format, return this
        if ($endPhone && strlen($endPhone) == 11 && is_numeric($endPhone)) {
            return $endPhone;
        }
        // Return phone by default
        if (!$endPhone) {
            return $phone;
        }
    }

    public function add(EmergencyAddRequest $request)
    {
        // Init data
        $angel = $this->angelRepository->getAngel();
        $aidants = $angel->aidants;
        
        // Save emergency alert
        $emergency = new Emergency;
        $emergency->ida = \Illuminate\Support\Str::random(10);
        $emergency->longitude = $request->longitude;
        $emergency->latitude = $request->latitude;
        $emergency->angel_id = $angel->id;
        $emergency->url = route($request->header('develop') ? 'api.develop.emergency.view' : 'api.emergency.view', $emergency->ida);
        $emergency->save();
                
        // Check if request has mailto to send at email instead of sms
        if ($request->mailto) {
            $messageContent = "🚨 Bonjour ".ucfirst($aidant->prenom).",\n".ucfirst($angel->prenom)." a lancé un appel d'urgence.\n\nEn savoir plus:\n".$emergency->url;
            \Illuminate\Support\Facades\Mail::to($request->mailto)->send(new EmergencyAlertEmail($messageContent));
        } elseif ($emergency && count($aidants) > 0) {
            // Init Vonage
            $basic  = new \Vonage\Client\Credentials\Basic("ca24576e", "8dcfT0zBCAmqy1b8");
            $client = new \Vonage\Client($basic);
            
            // Init messages
            $messageContent = "🚨 Bonjour ".ucfirst($aidant->prenom).",\n".ucfirst($angel->prenom)." a lancé un appel d'urgence.\n\nEn savoir plus:\n".$emergency->url;
            $tels = json_decode($angel->fiche_medicale->tels);

            if (isset($tels) && count($tels) > 0) {
                // Send sms to medical form's phones
                foreach ($tels as $tel) {
                    $response = $client->sms()->send(
                        new \Vonage\SMS\Message\SMS($this->phoneFormat($tel), 'Angel', $messageContent)
                    );
                }
            } else {
                // Send sms to aidant's phone
                foreach ($aidants as $aidant) {
                    $response = $client->sms()->send(
                        new \Vonage\SMS\Message\SMS($this->phoneFormat($aidant->telephone), 'Angel', $messageContent)
                    );
                }
            }

            // Check if message has sended
            $message = $response->current();
            if ($message->getStatus() != 0) {
                return $this->responseError('EMERGENCY_ADD_SMS_FAILED');
            }
        }

        return $emergency;
    }
}
