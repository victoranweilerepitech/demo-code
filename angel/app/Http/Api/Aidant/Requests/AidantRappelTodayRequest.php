<?php

namespace App\Http\Api\Aidant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AidantRappelTodayRequest extends FormRequest
{
    public function rules() {
        return [
            'angel_id' => ['required'],
        ];
    }

    public function messages() {
        return [
            'angel_id.required' => 'AIDANT_RAPPEL_ANGEL_ID_REQUIRED',
        ];
    }
}
