<?php

namespace App\Http\Api\Aidant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AidantAngelLogoutRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'angel_id' => ['required'],
        ];
    }

    public function messages() {
        return [
            'angel_id.required' => 'AIDANT_ANGEL_ID_REQUIRED',
        ];
    }
}
