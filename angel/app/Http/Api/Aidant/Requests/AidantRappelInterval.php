<?php

namespace App\Http\Api\Aidant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AidantRappelInterval extends FormRequest
{
    public function rules() {
        return [
            'start' => ['required'],
            'end' => ['required'],
        ];
    }

    public function messages() {
        return [
            'start.required' => 'AIDANT_RAPPEL_START_REQUIRED',
            'end.required' => 'AIDANT_RAPPEL_END_REQUIRED',
        ];
    }
}
