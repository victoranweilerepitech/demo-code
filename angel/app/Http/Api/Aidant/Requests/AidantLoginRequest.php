<?php

namespace App\Http\Api\Aidant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AidantLoginRequest extends FormRequest
{
    public function rules() {
        return [
            'email' => ['required'],
            'password' => ['required']
        ];
    }

    public function messages() {
        return [
            'email.required' => 'FORM_AIDANT_LOGIN_EMAIL_REQUIRED',
            'password.required' => 'FORM_AIDANT_LOGIN_PASSWORD_REQUIRED'
        ];
    }
}
