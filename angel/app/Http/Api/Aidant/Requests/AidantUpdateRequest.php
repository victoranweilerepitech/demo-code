<?php

namespace App\Http\Api\Aidant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AidantUpdateRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'nom' => ['alpha', 'nullable'],
            'prenom' => ['alpha', 'nullable'],
            'email' => ['unique:aidants', 'nullable'],
            'password' => ['nullable'],
            'telephone' => ['regex:/^((\+)33|0|0033)[1-9](\d{2}){4}$/', 'nullable'],
            'langue' => ['nullable'],
        ];
    }

    public function messages() {
        return [
            'nom.alpha' => 'AIDANT_UPDATE_NOM_FORMAT',
            'prenom.alpha' => 'AIDANT_UPDATE_PRENOM_FORMAT',
            'email.unique' => 'AIDANT_UPDATE_EMAIL_UNIQUE',
            'telephone.regex' => 'AIDANT_UPDATE_TELEPHONE_FORMAT',
        ];
    }
}
