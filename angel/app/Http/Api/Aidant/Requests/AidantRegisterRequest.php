<?php

namespace App\Http\Api\Aidant\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AidantRegisterRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'nom' => ['required'],
            'prenom' => ['required'],
            'email' => ['required', 'unique:aidants'],
            'password' => ['required'],
            'telephone' => ['required', 'regex:/^((\+)33|0|0033)[1-9](\d{2}){4}$/'],
            'langue' => ['nullable']
        ];
    }

    public function messages() {
        return [
            'nom.required' => 'AIDANT_REGISTER_NOM_REQUIRED',
            'prenom.required' => 'AIDANT_REGISTER_PRENOM_REQUIRED',
            'email.required' => 'AIDANT_REGISTER_EMAIL_REQUIRED',
            'email.unique' => 'AIDANT_REGISTER_EMAIL_UNIQUE',
            'password.required' => 'AIDANT_REGISTER_PASSWORD_REQUIRED',
            'telephone.required' => 'AIDANT_REGISTER_TELEPHONE_REQUIRED',
            'telephone.regex' => 'AIDANT_REGISTER_TELEPHONE_FORMAT'
        ];
    }
}
