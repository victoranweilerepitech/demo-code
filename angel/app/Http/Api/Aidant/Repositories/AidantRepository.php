<?php

namespace App\Http\Api\Aidant\Repositories;

use App\Http\Api\DefaultToolBox;
use Illuminate\Support\Facades\Auth;

// Models
use App\Models\Angel;
use App\Models\Aidant;
use App\Models\Actualite;

// Requests
use App\Http\Api\Angel\Requests\AngelUpdateRequest;
use App\Http\Api\Aidant\Requests\AidantUpdateRequest;
use App\Http\Api\Angel\Requests\AngelRegisterRequest;
use App\Http\Api\Aidant\Requests\AidantRegisterRequest;
use App\Http\Api\Angel\Requests\AngelUpdateSettingRequest;

class AidantRepository 
{
    use DefaultToolBox;

    /* ==================
        Aidant account
    ===================*/
    public function getAidant() : ?Aidant {
        return auth()->user();
    }

    public function getAidantByEmailAndPassword(String $email, String $password) : ?Aidant {
        // Check aidant auth
        if (Auth::guard('aidant')->attempt(['email' => $email, 'password' => $password])) {
            return auth()->guard('aidant')->user();
        }
        return null;
    }

    public function create(AidantRegisterRequest $request) : ?Aidant
    {
        // Prepare request fields
    	$request['nom_complet'] = $request->prenom.' '.$request->nom;
    	$request['password'] = bcrypt($request->password);

        // Save aidant
    	$aidant = new Aidant;
    	$aidant->fill($request->all());
    	$aidant->save();

    	return $aidant;
    }

    public function update(AidantUpdateRequest $request)
    {
        // Init data
        $aidant = $this->getAidant();
        $data = $request->validated();
        
        // Prepare request fields
        if (!$aidant)
            return null;
        if ($request['password'])
            $data['password'] = bcrypt($data['password']);
        if ($request['nom'] && $request['prenom'])
            $data['nom_complet'] = $data['prenom'].' '.$data['nom'];
        elseif ($request['nom'])
            $data['nom_complet'] = $aidant['prenom'].' '.$data['nom'];
        elseif ($request['prenom'])
            $data['nom_complet'] = $data['prenom'].' '.$aidant['nom'];

        // Update aidant account
        $aidant->fill($data);
    	$aidant->save();

        return $aidant;
    }

    public function delete()
    {
        // Get aidant
        $aidant = $this->getAidant();
        
        // Dettach all angels
        $aidant->angels()->detach([]);

        // Delete aidant account
        $aidant->delete();

        return true;
    }

    /* ================
        Angel Account
    =================*/
    public function addAngelInAccount(AngelRegisterRequest $request)
    {
        // Set default color
        $color = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);

        // Prepare request fields
        $request['nom_complet'] = $request->prenom.' '.$request->nom;
        $request['password'] = bcrypt($request->password);
        $request['couleur'] = $color;
        $request['avatar'] = 'https://sbcf.fr/wp-content/uploads/2018/03/sbcf-default-avatar.png';

        // Save angel in db
        $angel = new Angel;
        $angel->fill($request->all());
        $angel->save();

        // Create angel's medical form
        $ficheMedicale = new \App\Models\FicheMedicale;
        $ficheMedicale->angel_id = $angel->id;
        $ficheMedicale->save();

        // Create settings keywords
        $settingKeywords = \App\Models\AngelSettingKeyword::all();
        foreach($settingKeywords as $keyword) {
            $setting = new \App\Models\AngelSetting;
            $setting->keyname = $keyword->key;
            $setting->value = $keyword->default;
            $setting->angel_id = $angel->id;
            $setting->save();
        }

        // Attach angel to the aidant
        $aidant = $this->getAidant();
        $angel->aidants()->attach($aidant);

        return $angel->load('settings', 'fiche_medicale');
    }
    
    public function removeAngelFromAccount(Angel $angel)
    {
        // Get aidant
        $aidant = $this->getAidant();

        // If I can't delete angel
        if (!$aidant || !$this->isMyAngel($angel))
            return null;
        
        // Dettach angel account
        return $angel->aidants()->detach($aidant);
    }

    public function updateAngelProfil(AngelUpdateRequest $request, Angel $angel)
    {
        // Init data
        $aidant = $this->getAidant();
        $data = $request->validated();

        // Validations
        if (!$aidant) {
            $this->responseError('AIDANT_NOT_FOUND');
        }
        if (!$this->isMyAngel($angel)) {
            $this->responseError('AIDANT_NOT_AUTHORIZE');
        }

        // Prepare request fields
        $request['avatar'] = 'https://sbcf.fr/wp-content/uploads/2018/03/sbcf-default-avatar.png';
        if ($request['password'])
            $data['password'] = bcrypt($data['password']);
        if ($request['nom'] && $request['prenom'])
            $data['nom_complet'] = $data['prenom'].' '.$data['nom'];
        elseif ($request['nom'])
            $data['nom_complet'] = $aidant['prenom'].' '.$data['nom'];
        elseif ($request['prenom'])
            $data['nom_complet'] = $data['prenom'].' '.$aidant['nom'];

        // Update angel in db
        $angel->fill($data);
        $angel->save();      

        return $angel;
    }

    public function updateAngelSetting(Angel $angel, AngelUpdateSettingRequest $request)
    {
        // Get setting
        $setting = \App\Models\AngelSetting::find($request->id);

        // Check if setting exists
        if (!$setting) {
            return $this->responseError('ANGEL_SETTING_NOT_FOUND');
        }

        // Check if id is on the right angel
        if ($setting->angel->id != $angel->id) {
            return $this->responseError('BAD_ANGEL_ID');
        }

        // Check if is my angel
        if (!$this->isMyAngel($angel)) {
            return $this->responseError('ANGEL_NOT_AUTHORIZE');
        }

        // Update setting
        $setting->value = $request->value;
        $setting->save();

        return $setting;
    }

    /* ================
        Angel
    =================*/

    public function isMyAngel(Angel $angel)
    {
        $aidant = $this->getAidant();
        return in_array($angel->id, $aidant->angels->pluck('id')->toArray());
    }

    public function getAngels()
    {
        return $this->getAidant()->angels->load('fiche_medicale', 'settings');
    }

    /* ================
        Angel Auth
    =================*/

    public function sendAngelLogoutSocket(Angel $angel)
    {
        // Check if angel exists
        if (!$angel) {
            return $this->responseError('AIDANT_ANGEL_NOT_FOUND');
        }

        // Check if is my angel
        if (!$this->isMyAngel($angel)) {
            return $this->responseError('AIDANT_ANGEL_NOT_AUTHORIZE');
        }

        // Check if angel is connected
        if (!$angel->connected) {
            return $this->responseError('AIDANT_ANGEL_NOT_ON_APP');
        }

        // Send socket
        event(new \App\Events\LogoutAngelEvent($angel));
        return 1;
    }

    public function createAngelConnexionToken(Angel $angel)
    {
        // Check if angel exists
        if (!$angel) {
            return $this->responseError('AIDANT_ANGEL_NOT_FOUND');
        }

        // Check if is my angel
        if (!$this->isMyAngel($angel)) {
            return $this->responseError('AIDANT_ANGEL_NOT_AUTHORIZE');
        }

        // Create token
        $token = new \App\Models\LoginToken;
        $token->token = $this->randomToken(6).'-'.$this->randomToken(6).'-'.$this->randomToken(6);
        $token->url = route('api.angel.exchange-login-token', ['token' => $token->token]);
        $token->expire_at = \Carbon\Carbon::now()->addHour()->timestamp;
        $token->angel_id = $angel->id;
        $token->save();

        return $token;
    }

    /* ================
        Newsfeed
    =================*/

    public function getNewsFeed(?Angel $angel)
    {
        // Check if news feed is for all angels or only one
        if ($angel) {
            return Actualite::where('angel_id', $angel->id)->get();
        } else {
            $aidant = $this->getAidant();
            $angelIds = $aidant->angels->pluck('id');
            return Actualite::whereIn('angel_id', $angelIds)
                            ->orWhere('aidant_id', $aidant->id)
                            ->with('angel', 'aidant')
                            ->get();
        }
    }
}
