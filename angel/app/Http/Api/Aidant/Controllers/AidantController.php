<?php

namespace App\Http\Api\Aidant\Controllers;

use App\Http\Api\Controllers\ApiController;

// Models
use App\Models\Angel;
use App\Models\Rappel;

// Requests
use App\Http\Api\Rappel\Requests\RappelAddRequest;
use App\Http\Api\Angel\Requests\AngelUpdateRequest;
use App\Http\Api\Angel\Requests\RappelAngelRequest;
use App\Http\Api\Aidant\Requests\AidantLoginRequest;
use App\Http\Api\Aidant\Requests\AidantUpdateRequest;
use App\Http\Api\Angel\Requests\AngelRegisterRequest;
use App\Http\Api\Rappel\Requests\RappelUpdateRequest;
use App\Http\Api\Aidant\Requests\AidantRappelInterval;
use App\Http\Api\FicheMedicale\Requests\FicheMedicaleIdRequest;
use App\Http\Api\Actualite\Requests\GetNewsFeedRequest;
use App\Http\Api\Aidant\Requests\AidantRegisterRequest;
use App\Http\Api\Aidant\Requests\AidantRappelTodayRequest;
use App\Http\Api\Angel\Requests\AngelUpdateSettingRequest;

// Repositories
use App\Http\Api\Aidant\Repositories\AidantRepository;
use App\Http\Api\Rappel\Repositories\RappelRepository;
use App\Http\Api\Angel\Repositories\AngelRepository;
use App\Http\Api\FicheMedicale\Repositories\FicheMedicaleRepository;

class AidantController extends ApiController
{
    public function __construct() {
        $this->aidantRepository = new AidantRepository;
        $this->rappelRepository = new RappelRepository;
        $this->angelRepository = new AngelRepository;
        $this->ficheMedicaleRepository = new FicheMedicaleRepository;
    }

    /* ===================
        Aidant auth
    ====================*/

    public function login(AidantLoginRequest $request) {
        // Get Aidant
        $aidant = $this->aidantRepository->getAidantByEmailAndPassword($request->email, $request->password);
        if ($aidant) {
            // Create oauth token
            $user = $request->user('aidant');
            $tokenResult = $user->createToken('Aidant Access Token');
            $token = $tokenResult->token;
            $token->save();
            return response()->json([
                'aidant' => $aidant,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
            ]);
        }
        return $this->responseError('AIDANT_LOGIN_NOT_FOUND');
    }

    public function register(AidantRegisterRequest $request)
    {
        return $this->aidantRepository->create($request);
    }

    /* ================
        Account
    =================*/

    public function updateAccount(AidantUpdateRequest $request)
    {
        return $this->aidantRepository->update($request);
    }

    public function getAccount() {
        return $this->aidantRepository->getAidant();
    }

    public function deleteAccount()
    {
        return $this->aidantRepository->delete();
    }

    /* ================
        Rappels
    =================*/

    public function getRappelsToday(AidantRappelTodayRequest $request) {
        $aidant = $this->aidantRepository->getAidant();
        return $this->rappelRepository->getRappelsToday($aidant, $request->angel_id);
    }

    public function getRappelsInterval(AidantRappelInterval $request)
    {
        $aidant = $this->aidantRepository->getAidant();
        return $this->rappelRepository->getRappelsInterval($aidant, $request->start, $request->end, $request->angel_id, $request->sort);
    }

    public function getRappelsByAngel(RappelAngelRequest $request)
    {
        return $this->rappelRepository->getRappelsByAngel($request->angel_id);
    }

    public function deleteRappel(Rappel $rappel)
    {
        return $this->rappelRepository->deleteRappel($rappel);
    }

    public function createRappel(RappelAddRequest $request)
    {
        return $this->rappelRepository->createRappel($request);
    }

    public function updateRappel(RappelUpdateRequest $request, Rappel $rappel)
    {
        return $this->rappelRepository->updateRappel($request, $rappel->id);
    }

    /* ================
        Angel
    =================*/

    public function getAngels() {
        return $this->aidantRepository->getAngels();
    }

    public function createAngel(AngelRegisterRequest $request)
    {
        return $this->aidantRepository->addAngelInAccount($request);
    }

    public function removeAngelFromAccount(Angel $angel)
    {
        return $this->aidantRepository->removeAngelFromAccount($angel);
    }

    public function updateAngel(AngelUpdateRequest $request, Angel $angel)
    {
        return $this->aidantRepository->updateAngelProfil($request, $angel);
    }

    public function updateAngelSetting(AngelUpdateSettingRequest $request, Angel $angel)
    {
        return $this->aidantRepository->updateAngelSetting($angel, $request);
    }

    /* ================
        Angel auth
    =================*/

    public function logoutAngel(Angel $angel)
    {
        return $this->aidantRepository->sendAngelLogoutSocket($angel);
    }

    public function createAngelLoginToken(Angel $angel)
    {
        return $this->aidantRepository->createAngelConnexionToken($angel);
    }

    /* ================
        Newsfeed
    =================*/

    public function getNewsFeed(GetNewsFeedRequest $request)
    {
        // Check if is my angel
        $angel = $this->angelRepository->getAngelById($request->angel_id);
        if ($angel && !$this->aidantRepository->isMyAngel($angel)) {
            return $this->responseError('ANGEL_NOT_AUTHORIZE');
        }

        return $this->aidantRepository->getNewsFeed($angel);
    }

    /* ================
        Medical Form
    =================*/
    public function getMedicalForm(Angel $angel)
    {
        // Check if is my angel
        if (!$this->aidantRepository->isMyAngel($angel)) {
            return $this->responseError('ANGEL_NOT_AUTHORIZE');
        }

        return $this->ficheMedicaleRepository->getFicheMedicale($angel);
    }

    public function updateMedicalForm(FicheMedicaleIdRequest $request, Angel $angel)
    {
        // Check if is my angel
        if (!$this->aidantRepository->isMyAngel($angel)) {
            return $this->responseError('ANGEL_NOT_AUTHORIZE');
        }

        return $this->ficheMedicaleRepository->update($request, $angel);
    }
}
