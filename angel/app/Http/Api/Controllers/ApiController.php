<?php

namespace App\Http\Api\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Api\DefaultToolBox;
use App\Models\Actualite;

class ApiController extends Controller
{
    use DefaultToolBox;

    public function addActualite($info, $angelId, $aidantId)
    {
        if ($info && ($angelId || $aidantId)) {
            $actu = new Actualite;
            $actu->info = $info;
            $actu->angel_id = $angelId;
            $actu->aidant_id = $aidantId;
            $actu->save();
            
            return $actu;
        }
        return 'missing data';
    }
}
