<?php

namespace App\Http\Api\Angel\Repositories;

use App\Http\Api\DefaultToolBox;
use App\Models\Angel;

use Illuminate\Support\Facades\Auth;

class AngelRepository
{
    use DefaultToolBox;

    public function getAngel() : ?Angel {
        $angel = auth()->user();
        return $angel->load('settings');
    }

    public function getAngelByEmailAndPassword(String $email, String $password) {
        // Check angel auth
        if (Auth::guard('angel')->attempt(['email' => $email, 'password' => $password])) {
            return auth()->guard('angel')->user();
        }
    }

    public function exchangeLoginToken($token)
    {
        // Get token data
        $loginToken = \App\Models\LoginToken::where('token', $token)->first();
        if (!$loginToken) {
            return $this->responseError('AIDANT_TOKEN_NOT_FOUND');
        }

        // Convert token to access token
        $angel = $loginToken->angel;
        if ($angel) {
            // Create oauth token
            $tokenResult = $angel->createToken('Angel Access Token');
            $token = $tokenResult->token;
            $token->save();

            // Delete temporary token
            $loginToken->delete();

            // Return login data
            return response()->json([
                'angel' => $angel,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
            ]);
        }
    }

    public function getAngelById(?int $angelId) : ?Angel {
        return Angel::find($angelId);
    }

    public function loginApp()
    {
        // Get angel
        $angel = $this->getAngel();
        // Set angel connected
        if ($angel) {
            $angel->connected = 1;
            $angel->save();
        }
        return $angel;
    }

    public function logoutApp()
    {
        // Get angel
        $angel = $this->getAngel();
        // Set angel disconnected
        if ($angel) {
            $angel->connected = 0;
            $angel->save();
        }

        return $angel;
    }
}
