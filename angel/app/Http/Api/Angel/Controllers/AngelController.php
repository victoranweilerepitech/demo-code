<?php

namespace App\Http\Api\Angel\Controllers;
use App\Http\Api\Controllers\ApiController;


// Models
use App\Models\Rappel;

// Repositories
use App\Http\Api\Angel\Repositories\AngelRepository;
use App\Http\Api\Rappel\Repositories\RappelRepository;
use App\Http\Api\FicheMedicale\Repositories\FicheMedicaleRepository;

// Requests
use App\Http\Api\Angel\Requests\AngelLoginRequest;
use App\Http\Api\Angel\Requests\AngelExchangeTokenRequest;

class AngelController extends ApiController
{
    public function __construct() {
        $this->angelRepository = new AngelRepository;
        $this->rappelRepository = new RappelRepository;
        $this->ficheMedicaleRepository = new FicheMedicaleRepository;
    }

    /* ================
        Auth
    =================*/

    public function login(AngelLoginRequest $request) {
        // Get angel account
        $angel = $this->angelRepository->getAngelByEmailAndPassword($request->email, $request->password);
        // Create token
        if ($angel) {
            $user = $request->user('angel');
            $tokenResult = $user->createToken('Angel Access Token');
            $token = $tokenResult->token;
            $token->save();
            
            // Save in newsfeed
            $this->addActualite("Votre angel ".$angel->nom_complet." s'est connecté", $angel->id, null);

            // Return login data
            return response()->json([
                'angel' => $angel,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
            ]);
        }
        return $this->responseError('ANGEL_LOGIN_NOT_FOUND');
    }

    public function loginApp() {
        return $this->angelRepository->loginApp();
    }

    public function logoutApp() {
        return $this->angelRepository->logoutApp();
    }

    public function exchangeLoginToken(AngelExchangeTokenRequest $request)
    {
        return $this->angelRepository->exchangeLoginToken($request->token);
    }

    /* ================
        Account
    =================*/

    public function getAccount() {
        return $this->angelRepository->getAngel();
    }

    /* ================
        Rappels
    =================*/

    public function getRappels() {
        return $this->angelRepository->getAngel()->rappels->load('file');
    }

    public function getRappelsByDate(String $date) {
        return $this->rappelRepository->getRappelsByDate($date);
    }

    public function callbackReceive(Rappel $rappel) {
        return $this->rappelRepository->callbackReceive($rappel);
    }

    /* ================
        Medical form
    =================*/
    public function getFicheMedicale()
    {
        $angel = $this->angelRepository->getAngel();
        return $this->ficheMedicaleRepository->getFicheMedicale($angel);
    }
}
