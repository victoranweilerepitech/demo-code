<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AngelRegisterRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'nom' => ['required'],
            'prenom' => ['required'],
            'email' => ['required', 'unique:angels'],
            'password' => ['required'],
            'telephone' => ['required', 'regex:/^((\+)33|0|0033)[1-9](\d{2}){4}$/'],
        ];
    }

    public function messages() {
        return [
            'nom.required' => 'ANGEL_REGISTER_NOM_REQUIRED',
            'prenom.required' => 'ANGEL_REGISTER_PRENOM_REQUIRED',
            'email.required' => 'ANGEL_REGISTER_EMAIL_REQUIRED',
            'email.unique' => 'ANGEL_REGISTER_EMAIL_UNIQUE',
            'password.required' => 'ANGEL_REGISTER_PASSWORD_REQUIRED',
            'telephone.required' => 'ANGEL_REGISTER_TELEPHONE_REQUIRED',
            'telephone.regex' => 'ANGEL_REGISTER_TELEPHONE_FORMAT',
        ];
    }
}
