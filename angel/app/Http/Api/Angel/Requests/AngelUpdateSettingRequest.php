<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AngelUpdateSettingRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'id' => ['required'],
            'value' => ['required'],
        ];
    }

    public function messages() {
        return [
            'id.required' => 'ANGEL_SETTING_ID_REQUIRED',
            'value.required' => 'ANGEL_SETTING_VALUE_REQUIRED',
        ];
    }
}
