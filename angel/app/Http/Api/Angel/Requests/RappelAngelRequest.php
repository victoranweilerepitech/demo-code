<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RappelAngelRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'angel_id' => ['required'],
        ];
    }

    public function messages() {
        return [
            'angel_id.required' => 'RAPPEL_ANGEL_ID_REQUIRED',
        ];
    }
}
