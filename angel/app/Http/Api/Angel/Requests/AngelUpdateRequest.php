<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AngelUpdateRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'nom' => ['alpha', 'nullable'],
            'prenom' => ['alpha', 'nullable'],
            'email' => ['unique:angels', 'nullable'],
            'password' => ['nullable'],
            'telephone' => ['regex:/^((\+)33|0|0033)[1-9](\d{2}){4}$/', 'nullable'],
        ];
    }

    public function messages() {
        return [
            'nom.alpha' => 'ANGEL_UPDATE_NOM_FORMAT',
            'prenom.alpha' => 'ANGEL_UPDATE_PRENOM_FORMAT',
            'email.unique' => 'ANGEL_UPDATE_EMAIL_UNIQUE',
            'telephone.regex' => 'ANGEL_UPDATE_TELEPHONE_FORMAT'
        ];
    }
}
