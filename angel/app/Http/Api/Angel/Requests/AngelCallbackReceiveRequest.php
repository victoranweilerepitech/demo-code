<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AngelCallbackReceiveRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'rappel_id' => ['required'],
        ];
    }

    public function messages() {
        return [
            'rappel_id.required' => 'ANGEL_RAPPEL_ID_REQUIRED',
        ];
    }
}
