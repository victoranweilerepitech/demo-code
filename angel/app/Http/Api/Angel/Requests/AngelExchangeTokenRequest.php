<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AngelExchangeTokenRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'token' => ['required'],
        ];
    }

    public function messages() {
        return [
            'token.required' => 'AIDANT_TOKEN_REQUIRED',
        ];
    }
}
