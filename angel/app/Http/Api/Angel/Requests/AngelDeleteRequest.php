<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AngelDeleteRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
        ];
    }

    public function messages() {
        return [
        ];
    }
}
