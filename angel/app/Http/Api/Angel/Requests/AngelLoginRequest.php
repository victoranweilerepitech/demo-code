<?php

namespace App\Http\Api\Angel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AngelLoginRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'email' => ['required'],
            'password' => ['required']
        ];
    }

    public function messages() {
        return [
            'email.required' => 'ANGEL_LOGIN_EMAIL_REQUIRED',
            'password.required' => 'ANGEL_LOGIN_PASSWORD_REQUIRED'
        ];
    }
}
