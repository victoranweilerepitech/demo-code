<?php

namespace App\Http\Api\Rappel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RappelAddRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'titre' => ['required'],
            'debut' => ['required'],
            'fin' => ['nullable'],
            'angel_id' => ['required'],
        ];
    }

    public function messages() {
        return [
            'titre.required' => 'RAPPEL_CREATE_TITLE_REQUIRED',
            'debut.required' => 'RAPPEL_CREATE_DEBUT_REQUIRED',
            'debut.integer' => 'RAPPEL_CREATE_DEBUT_TIMESTAMP',
            'debut.integer' => 'RAPPEL_CREATE_DEBUT_TIMESTAMP',
            'angel_id.required' => 'RAPPEL_CREATE_ANGEL_ID_REQUIRED',
        ];
    }
}
