<?php

namespace App\Http\Api\Rappel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoutineUpdateRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'duree' => ['numeric', 'nullable'],
            'type' => ['nullable'],
            'debut' => ['numeric', 'nullable'],
            'fin' => ['numeric', 'nullable'],
        ];
    }

    public function messages() {
        return [
            'debut.numeric' => 'ROUTINE_UPDATE_DEBUT_FORMAT',
            'fin.numeric' => 'ROUTINE_UPDATE_FIN_FORMAT',
            'duree.numeric' => 'ROUTINE_UPDATE_DUREE_FORMAT',
        ];
    }
}
