<?php

namespace App\Http\Api\Rappel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RappelIntervalRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'start_day' => ['required'],
            'end_day' => ['required']
        ];
    }

    public function messages() {
        return [
            'start_day.required' => 'RAPPEL_INTERVAL_START_DAY_REQUIRED',
            'end_day.required' => 'RAPPEL_INTERVAL_END_DAY_REQUIRED'
        ];
    }
}
