<?php

namespace App\Http\Api\Rappel\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RappelUpdateRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'titre' => ['nullable'],
            'description' => ['nullable'],
            'jour' => ['nullable', 'date_format:Y-d-m'],
            'heure' => ['nullable', 'date_format:H:i'],
            'angel_id' => ['nullable'],
        ];
    }

    public function messages() {
        return [
            'jour.date' => 'RAPPEL_UPDATE_JOUR_FORMAT',
            'heure.date_format' => 'RAPPEL_UPDATE_HEURE_FORMAT'
        ];
    }
}
