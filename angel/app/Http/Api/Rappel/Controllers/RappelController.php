<?php

namespace App\Http\Api\Rappel\Controllers;

use Carbon\Carbon;

use App\Models\Rappel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Api\Controllers\ApiController;
use App\Http\Api\Rappel\Requests\RappelAddRequest;
use App\Http\Api\Angel\Repositories\AngelRepository;

use App\Http\Api\Rappel\Requests\RappelUpdateRequest;
use App\Http\Api\Aidant\Repositories\AidantRepository;
use App\Http\Api\DefaultToolBox;
use App\Http\Api\Rappel\Repositories\RappelRepository;
use App\Http\Api\Rappel\Requests\RoutineUpdateRequest;

class RappelController extends ApiController
{
    use DefaultToolBox;

    private $angelRepository;
    private $aidantRepository;
    private $rappelRepository;

    public function __construct() {
        $this->angelRepository = new AngelRepository;
        $this->aidantRepository = new AidantRepository;
        $this->rappelRepository = new RappelRepository;
    }

    public function createRappel(RappelAddRequest $request) {
        $angel = $this->angelRepository->getAngelById($request->angel_id);

        if (!$angel) {
            return $this->responseError('ANGEL_NOT_FOUND');
        }

        $request['aidant_id'] = $this->aidantRepository->getAidant()->id;

        // Check if rappel is created in the past
        if (Carbon::createFromTimestamp($request->debut)->isPast()) {
            return $this->responseError('CREATE_RAPPEL_PAST');
        }

        $rappel = $this->rappelRepository->createRappel($request);

        if ($rappel) {
            // Save actualite
            $aidant = auth()->user();
            $this->addActualite($aidant->nom_complet." a créé le rappel ".$rappel->titre." à ".$angel->nom_complet, $angel->id, null);

            return $rappel;
        }

    }

    public function updateRappel(RappelUpdateRequest $request, int $rappelId)
    {
        return $this->rappelRepository->updateRappel($request, $rappelId);
    }

    public function updateRoutine(RoutineUpdateRequest $request, int $routineId)
    {
        return $this->rappelRepository->updateRoutine($request, $routineId);
    }

    public function deleteRoutine(int $routineId)
    {
        return $this->rappelRepository->deleteRoutine($routineId);
    }

    public function readRappel(int $rappelId)
    {
        $rappel = $this->rappelRepository->getRappelById($rappelId);
        return $this->rappelRepository->updateRappelState($rappel, 1);
    }

    public function validation(Request $request, int $rappelId) {
        $rappel = $this->rappelRepository->getRappelById($rappelId);
        $angel = $this->angelRepository->getAngel();

        $this->rappelRepository->validateRappel($rappel);
        // $this->rappelRepository->sendNextRappel($angel);
        $this->addActualite($angel->nom_complet." a validé le rappel ".$rappel->titre, $angel->id, null);
        return $rappel;
    }

    public function cancel(Request $request, int $rappelId) {
        $rappel = $this->rappelRepository->getRappelById($rappelId);
        $angel = $this->angelRepository->getAngel();

        $this->rappelRepository->closeRappel($rappel);
        // $this->rappelRepository->sendNextRappel($angel);
        $this->addActualite($angel->nom_complet." a annulé le rappel ".$rappel->titre, $angel->id, null);
        return $rappel;
    }

    public function receive(Request $request, int $rappelId) {
        $rappel = $this->rappelRepository->getRappelById($rappelId);
        $angel = $this->angelRepository->getAngel();

        $rappel = $this->rappelRepository->updateRappelState($rappel, 1);
        event(new \App\Events\RappelUpdatedEvent($rappel));
        return $rappel;
    }

    public function deleteRappel(Rappel $rappel) {
        $aidant = auth()->user();
        if (isset($rappel)) {
            if ($rappel->etat) {
                return $this->responseError('DELETE_ALERTED_RAPPEL');
            }
            // $this->addActualite($aidant->nom_complet." a supprimé le rappel ".$rappel->titre, $rappel->angel->id, null);
            return $rappel->delete();
        }
        return $this->responseError('RAPPEL_NOT_FOUND');
    }
}
