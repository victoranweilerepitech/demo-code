<?php

namespace App\Http\Api\Rappel\Repositories;

use Carbon\Carbon;
use App\Http\Api\DefaultToolBox;
use Illuminate\Support\Collection;

// Repositories
use App\Http\Api\Aidant\Repositories\AidantRepository;
use App\Http\Api\Angel\Repositories\AngelRepository;

// Models
use App\Models\Angel;
use App\Models\Aidant;
use App\Models\Rappel;
use App\Models\Routine;

// Requests
use App\Http\Api\Rappel\Requests\RappelAddRequest;
use App\Http\Api\Rappel\Requests\RappelUpdateRequest;
use App\Http\Api\Rappel\Requests\RoutineUpdateRequest;

class RappelRepository
{
    use DefaultToolBox;

    public function __construct()
    {
        $this->aidantRepository = new AidantRepository;
        $this->angelRepository = new AngelRepository;
    }

    /* --------------------
    *
    *    GET RAPPELS
    *
    -------------------- */

    public function getRappelsByDate(String $date) : Collection {
        // Define time range
        $start = Carbon::createFromTimestamp($date)->startOfDay()->timestamp;
        $end = Carbon::createFromTimestamp($date)->endOfDay()->timestamp;

        // Get rappels
        return Rappel::whereBetween('debut', [$start, $end])
        ->orderBy('debut')
        ->with('file', 'angel', 'routine')
        ->get()
        ->makeHidden('routine_id');
    }

    public function getRappelById(int $rappelId) : ?Rappel {
        return Rappel::find($rappelId);
    }

    public function getRappelsToday(Aidant $aidant, $angelId = null)
    {
        // Define time range
        $start = \Carbon\Carbon::today()->timestamp;
        $end = \Carbon\Carbon::today()->addDay()->timestamp;
        // Create rappel query
        $queryRappels = \App\Models\Rappel::whereIn('angel_id', $aidant->angels->pluck('id')->toArray())
                                    ->where('debut', '>=', $start)
                                    ->where('fin', '<', $end);

        // Can filter by angel
        if ($angelId) {
            if (!in_array($angelId, $aidant->angels->pluck('id')->toArray())) {
                return $this->responseError('AIDANT_RAPPEL_ANGEL_NOT_AUTHORIZE');
            }
            $queryRappels->where('angel_id', $angelId);
        }

        // Get Rappels
        $rappels = $queryRappels->with('file', 'angel')->get();

        return $rappels;
    }

    public function getRappelsInterval(Aidant $aidant, $start, $end, $angelId = null, $sorted = false)
    {
        // Create routine's rappels
        if ($angelId) {
            $angel = Angel::find($angelId);
            $routines = $angel->rappels->whereNotNull('routine_id')->pluck('routine_id');

            // Create rappels in routine
            foreach ($routines as $routineId) {
                $routine = Routine::find($routineId);
                if ($routine && !$routine->fin) {
                    $this->createRappelsInRoutine($routine, $routine->rappels[0], $routine->rappels[0]->debut, $end, $routine->getConnectionName());
                }
            }
        }

        // Build Query
        $queryRappels = \App\Models\Rappel::whereIn('angel_id', $aidant->angels->pluck('id')->toArray())
                                    ->whereBetween('debut', [$start, $end]);
        
        // Can filter by angel
        if ($angelId) {
            if (!in_array($angelId, $aidant->angels->pluck('id')->toArray())) {
                return $this->responseError('AIDANT_RAPPEL_ANGEL_NOT_AUTHORIZE');
            }
            $queryRappels->where('angel_id', $angelId);
        }

        // Get query
        $rappels = $queryRappels->orderBy('debut')->with('file', 'angel', 'routine')->get();
        
        // Can sort response
        if ($sorted) {
            $rappelDays = [];
            $day = Carbon::createFromTimestamp($start)->startOfDay();
            $endDate = Carbon::createFromTimestamp($end)->endOfDay();
            for (; $day->timestamp < $endDate->timestamp; $day->addDay()) {
                $rappelDays[$day->format('d/m/y')] = $rappels->whereBetween('debut', [$day->startOfDay()->timestamp, $day->endOfDay()->timestamp]);
            }
            $rappelDays[$day->format('d/m/y')] = $rappels->whereBetween('debut', [$day->startOfDay()->timestamp, $day->endOfDay()->timestamp]);

            return $rappelDays;
        } else {
            return $rappels;
        }
    }

    public function getRappelsByAngel($angelId)
    {
        $angel = Angel::find($angelId);
        return $angel->rappels;
    }

    /* --------------------
    *
    *    ROUTINES
    *
    -------------------- */

    private function getNextTime($start, $type)
    {
        if ($type == 'daily')
            $next = Carbon::createFromTimestamp($start)->addDay()->timestamp;
        if ($type == 'weekly')
            $next = Carbon::createFromTimestamp($start)->addWeek()->timestamp;
        if ($type == 'monthly')
            $next = Carbon::createFromTimestamp($start)->addMonth()->timestamp;
        if ($type == 'yearly')
            $next = Carbon::createFromTimestamp($start)->addYear()->timestamp;
        return $next;
    }

    private function saveRoutine($rappelId, $type, $debut, $fin)
    {
        $routine = new Routine;
        $routine->rappel_id = $rappelId;
        $routine->type = $type;
        $routine->debut = $debut;
        $routine->fin = $fin;
        $routine->save();

        return $routine;
    }

    public function updateRoutine(RoutineUpdateRequest $request, int $routineId)
    {
        $routine = Routine::findOrFail($routineId);
        $data = $request->validated();
        $routine->fill($data);
        $routine->save();
        return $routine;
    }

    public function deleteRoutine(int $routineId)
    {
        $routine = Routine::findOrFail($routineId);
        if (isset($routine)) {
            Rappel::where('routine_id', $routine->id)->where('etat', 0)->delete();
            return $routine->delete();
        }
        return $this->responseError('ROUTINE_NOT_FOUND');
    }

    public function createRappelsInRoutine(Routine $routine, Rappel $rappel, $debut, $end, $connectionName = 'mysql')
    {
        $debuts = [];
        array_push($debuts, $debut);
        while ($debut <= $end) {
            $debut = $this->getNextTime($debut, $routine->type);
            array_push($debuts, $debut);
        }

        if ($routine->debut <= $debut) {
            foreach ($debuts as $debut) {
                $rappelExists = Rappel::on($connectionName)->where('routine_id', $routine->id)->where('debut', $debut)->first();
                if (!$rappelExists) {
                    $newRappel = new Rappel;
                    $rappel->debut = $debut;
                    $rappel->fin = $debut + ($rappel->fin - $rappel->debut);
                    $rappel->etat = 0;
                    $newRappel->setConnection($connectionName);
                    $newRappel->fill($rappel->toArray());
                    $newRappel->save();
                }
            }
        }
    }

    public function sendNextRappel(Angel $angel) {
        $waitingResponse = Rappel::where('angel_id', $angel->id)->where('etat', 1)->first();
        if (!$waitingResponse) {
            $rappel = Rappel::where('angel_id', $angel->id)
                            ->where('etat', 0)
                            ->orderBy('debut')
                            ->first();
            if ($rappel) {
                event(new \App\Events\SendRappelEvent($rappel, $angel));
                return 1;
            }
        }
        return 0;
    }

    /* --------------------
    *
    *    RAPPELS
    *
    -------------------- */

    public function createRappel(RappelAddRequest $request) {
        // Add aidant
        $request['aidant_id'] = $this->aidantRepository->getAidant()->id;

        // Check if rappel is created in the past
        if (Carbon::createFromTimestamp($request->debut)->isPast()) {
            return $this->responseError('CREATE_RAPPEL_PAST');
        }

        // Set default end
        if (!$request->fin) {
            $request['fin'] = Carbon::createFromTimestamp($request->debut)->addMinutes(30)->timestamp;
        }

        // Save rappel
        $rappel = new Rappel;
        $rappel->fill($request->all());
        $rappel->save();

        // Save file
        if ($request->file_ref) {
            $file = \App\Models\File::where('reference', $request->file_ref)->first();
            if ($file) {
                $rappel->file_ref = $request->file_ref;
                $rappel->save();
                $rappel->file = $file;
            }
        }

        // Save Routine
        if ($request->interval) {
            $routine = $this->saveRoutine(
                $rappel->id,
                $request->interval,
                $rappel->debut,
                $request->interval_fin
            );
            $rappel->routine_id = $routine->id;
            $rappel->save();

            if (!$request->interval_fin) {
                $request->interval_fin = Carbon::createFromTimestamp($rappel->debut)->addMonth()->timestamp;
            }
            $this->createRappelsInRoutine($routine, $rappel, $rappel->debut, $request->interval_fin, $routine->getConnectionName());
        }

        return $rappel;
    }

    public function updateRappel(RappelUpdateRequest $request, int $rappelId)
    {
        $rappel = $this->getRappelById($rappelId);
        $data = $request->all();

        if (isset($rappel)) {
            $rappel->fill($data);
            $rappel->save();

            if ($request->file_ref) {
                $file = \App\Models\File::where('reference', $request->file_ref)->first();
                if ($file) {
                    $rappel->file_ref = $request->file_ref;
                    $rappel->save();
                    $rappel->file = $file;
                }
            }
            return $rappel;
        }
        return $this->responseError('RAPPEL_NOT_FOUND');
    }

    public function updateRappelState(Rappel $rappel, int $state) : ?Rappel {
        $rappel->etat = $state;
        $rappel->save();
        return $rappel;
    }

    public function updateRappelsState(Collection $rappels, int $state) : Collection {
        foreach ($rappels as $rappel) {
            $this->updateRappelState($rappel, $state);
        }
        return $rappels;
    }

    public function validateRappel($rappel)
    {
        $rappel = $this->updateRappelState($rappel, 2);
        event(new \App\Events\RappelUpdatedEvent($rappel));
        // $this->sendNextRappel($rappel->angel);
    }

    public function closeRappel(Rappel $rappel)
    {
        $rappel = $this->updateRappelState($rappel, 3);
        if ($rappel) {
            event(new \App\Events\RappelUpdatedEvent($rappel));
            // $this->sendNextRappel($rappel->angel);
        }
    }

    public function deleteRappel(Rappel $rappel)
    {
        if ($rappel->etat) {
            return $this->responseError('DELETE_ALERTED_RAPPEL');
        }
        // $this->addActualite($aidant->nom_complet." a supprimé le rappel ".$rappel->titre, $rappel->angel->id, null);
        return $rappel->delete();
    }

    public function callbackReceive(Rappel $rappel)
    {
        $angel = $this->angelRepository->getAngel();

        if ($rappel->angel_id != $angel->id) {
            return $this->responseError('ANGEL_HASNT_THIS_RAPPEL');
        }
        return $this->updateRappelState($rappel, 1);
    }
}
