<?php

namespace App\Http\Api\FicheMedicale\Repositories;

use App\Models\Angel;
use App\Models\FicheMedicale;
use App\Http\Api\DefaultToolBox;

use App\Http\Api\FicheMedicale\Requests\FicheMedicaleIdRequest;

class FicheMedicaleRepository
{
    use DefaultToolBox;

    public function getFicheMedicale(Angel $angel)
    {
        // Return medical form if exists
        if ($angel->fiche_medicale) {
            return $angel->fiche_medicale;
        } else {
            // Create medical form
            $ficheMedicale = new FicheMedicale;
            $ficheMedicale->angel_id = $angel->id;
            $ficheMedicale->save();
            $ficheMedicale = $ficheMedicale->fresh();
            return $ficheMedicale;
        }
    }

    public function update(FicheMedicaleIdRequest $request, Angel $angel)
    {
        // Get fiche medicale
        $medicalForm = $this->getFicheMedicale($angel);

        // Update data
        $medicalForm->fill($request->all());
        $medicalForm->save();
        return $medicalForm;
    }
}
