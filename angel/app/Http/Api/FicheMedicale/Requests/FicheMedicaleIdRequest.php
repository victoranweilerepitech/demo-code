<?php

namespace App\Http\Api\FicheMedicale\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FicheMedicaleIdRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'angel_id' => ['required'],
        ];
    }

    public function messages() {
        return [
            'angel_id.required' => 'FICHE_MEDICAL_ANGEL_ID_REQUIRED',
        ];
    }
}
