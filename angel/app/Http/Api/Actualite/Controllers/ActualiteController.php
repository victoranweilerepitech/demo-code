<?php

namespace App\Http\Api\Actualite\Controllers;

use App\Http\Api\Controllers\ApiController;
use App\Models\Actualite;
use Illuminate\Http\Request;

class ActualiteController extends ApiController
{
    public function get(Request $request)
    {
        $aidant = auth()->user();
        $angelIds = $aidant->angels->pluck('id');

        if ($request->angel_id) {
            $actus = Actualite::where('angel_id', $request->angel_id)->get();
        } else {
            $actus = Actualite::whereIn('angel_id', $angelIds)
                            ->orWhere('aidant_id', $aidant->id)
                            ->with('angel', 'aidant')
                            ->get();
        }

        return $actus;
    }
}
