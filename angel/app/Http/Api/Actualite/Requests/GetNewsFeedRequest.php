<?php

namespace App\Http\Api\Actualite\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetNewsFeedRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'angel_id' => ['nullable'],
        ];
    }

    public function messages() {
        return [
        ];
    }
}
